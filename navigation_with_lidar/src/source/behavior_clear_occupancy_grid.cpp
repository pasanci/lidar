/*!*******************************************************************************************
 *  \file       behavior_clear_occupancy_grid.cpp
 *  \brief      Behavior Clear Occupancy Grid implementation file.
 *  \details    This file implements the BehaviorClearOccupancyGrid class.
 *  \authors    Raul Cruz, Pablo Santamaria
 *  \maintainer Pablo Santamaria
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/behavior_clear_occupancy_grid.h"
#include <pluginlib/class_list_macros.h>

namespace navigation_with_lidar_dev
{
BehaviorClearOccupancyGrid::BehaviorClearOccupancyGrid() : BehaviorExecutionController()
{
  hector_reset_msg.data = "reset";
  setName("clear_occupancy_grid");
}

BehaviorClearOccupancyGrid::~BehaviorClearOccupancyGrid()
{
}

bool BehaviorClearOccupancyGrid::checkSituation() 
{ 
 return true; 
}

void BehaviorClearOccupancyGrid::checkProgress() 
{ 
 
}

void BehaviorClearOccupancyGrid::checkProcesses() 
{ 
 
}

void BehaviorClearOccupancyGrid::checkGoal() 
{
  BehaviorExecutionController::setTerminationCause(aerostack_msgs::BehaviorActivationFinished::GOAL_ACHIEVED);
}
void BehaviorClearOccupancyGrid::onConfigure(){

  ros::NodeHandle nh = getNodeHandle();
  nh.param<std::string>("poseupdate", poseupdate_topic_str, "poseupdate");
  nh.param<std::string>("initialpose", initialpose_topic_str, "initialpose");
  nh.param<std::string>("estimated_pose_topic", self_localization_pose_str, "self_localization/pose");
  is_stopped = false;
}


void BehaviorClearOccupancyGrid::onActivate(){
  ros::NodeHandle nh = getNodeHandle();
  std::string nspace = getNamespace();
  world = *ros::topic::waitForMessage<geometry_msgs::PoseStamped>("/" + nspace + "/"+self_localization_pose_str, nh, ros::Duration(1));
  grid = *ros::topic::waitForMessage<geometry_msgs::PoseWithCovarianceStamped>("/" + nspace + "/"+poseupdate_topic_str, nh, ros::Duration(1));
  initial_pose_pub = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>("/" + nspace + "/" + initialpose_topic_str, 1);
  hectorReset = nh.advertise<std_msgs::String>("/" + nspace + "/" + "syscommand", 1000, true);
  is_stopped = false;
  grid.pose.pose = world.pose;
  initial_pose_pub.publish(grid);
  hectorReset.publish(hector_reset_msg);
}

void BehaviorClearOccupancyGrid::onDeactivate(){
  is_stopped = true;
}

void BehaviorClearOccupancyGrid::onExecute()
{
}

}
PLUGINLIB_EXPORT_CLASS(navigation_with_lidar_dev::BehaviorClearOccupancyGrid, nodelet::Nodelet)
